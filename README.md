## Wethos Tech Interview Project

Thank you for checking out my tech interview submission.

One thing I'd like to note is that the spec that was emailed to me was non-specific for implementation. I knew this and one thing that I could have done (should have done?) was clarify the spec with the stakeholders, but I chose not to do this. My reasoning was that, with this position being a backend position, whatever my choice of UI will probably not be of any consequence. In rolling the dice on that assumption, my UI of choice was to be the CLI, for better or worse. ¯\\\_(ツ)\_/¯

Thank you all for the opportunity.

Requirements
------------

* Requires PHP8

Installation
------------

Install with Composer ```composer install```

Configuration
-------------

Open up the .env file and make sure to configure the WETHOS_CLIENT_ID and WETHOS_CLIENT_SECRET are configured correctly.

Usage
-----

1. Run ```bin/console wethos:authenticate "<username>" "<password>"```.
2. Run ```bin/console wethos:profile``` to view the profile of the authenticated user.
3. Run ```bin/console wethos:projects``` to view a list of the user's projects.
4. Run ```bin/console wethos:projects "<project_id>"``` with an id that was listed in the projects command to view that project.

Todo
----

* Coulda, shoulda, woulda wrote unit tests.