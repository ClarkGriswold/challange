<?php declare(strict_types=1);

namespace App\Http\OAuth;

/**
 * Immutable value object.
 */
class AuthenticatePayload
{
    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $scope;

    /** @var string */
    private $grantType;

    public function __construct(string $clientId, string $clientSecret, string $username, string $password, string $scope = '*')
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->username     = $username;
        $this->password     = $password;
        $this->scope        = $scope;
        $this->grantType    = 'password';
    }

    public function toArray(): array
    {
        return [
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'username'      => $this->username,
            'password'      => $this->password,
            'scope'         => $this->scope,
            'grant_type'    => $this->grantType,
        ];
    }

    public function toJson(): string
    {
        return json_encode($this->toArray());
    }
}
