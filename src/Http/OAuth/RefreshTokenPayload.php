<?php declare(strict_types=1);

namespace App\Http\OAuth;

/**
 * Immutable value object.
 */
class RefreshTokenPayload
{
    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var string */
    private $refreshToken;

    /** @var string */
    private $scope;

    /** @var string */
    private $grantType;

    public function __construct(string $clientId, string $clientSecret, string $refreshToken, string $scope = '*' )
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->refreshToken = $refreshToken;
        $this->scope        = $scope;
        $this->grantType    = 'refresh_token';
    }

    public function toArray()
    {
        return [
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $this->refreshToken,
            'scope'         => $this->scope,
            'grant_type'    => $this->grantType,
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
