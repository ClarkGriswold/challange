<?php declare(strict_types=1);

namespace App\Http;

use App\Repository\TokenRepository;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Transport
{
    const HEADER_ACCEPT           = 'application/json,text/plain,*/*';
    const HEADER_X_REQUESTED_WITH = 'XMLHttpRequest';
    const HEADER_CONTENT_TYPE     = 'application/json';

    /** @var Client */
    private $client;

    /** @var TransportResponseHandler */
    private $transportResponseHandler;

    /** @var TokenRepository */
    private $tokenRepository;

    /** @var int */
    private $clientId;

    /** @var string */
    private $clientSecret;

    public function __construct(
        TokenRepository $tokenRepository,
        string $baseUri,
        string $clientId,
        string $clientSecret
    ) {
        if (!filter_var($baseUri, FILTER_VALIDATE_URL)) {
            throw new \Exception('A valid URL is required.');
        }

        if (!$clientId || !$clientSecret) {
            throw new \Exception('Missing client id or client secret. Make sure these are configured correctly in your .env file.');
        }

        $this->client          = new Client(['base_uri' => $baseUri]);
        $this->tokenRepository = $tokenRepository;
        $this->clientId        = $clientId;
        $this->clientSecret    = $clientSecret;
    }

    public function get(string $path, array $headers = []): array
    {
        $method   = 'GET';
        $options  = [RequestOptions::HEADERS => $headers];
        $response = $this->request($method, $path, $options);

        // Attemp refresh and one request retry.
        if ($response->getStatusCode() === SymfonyResponse::HTTP_UNAUTHORIZED && $this->attemptRefresh($options)) {
            $response = $this->request($method, $path, $options);
        }

        return $this->handleResponse($response);
    }

    public function post(string $path, array $body, $headers = []): array
    {
        $response = $this->request('POST', $path, [
            RequestOptions::HEADERS => $headers,
            RequestOptions::BODY    => json_encode($body),
        ]);

        return $this->handleResponse($response);
    }

    public function put()
    {
        throw new \Exception('Not implemented.');
    }

    public function delete()
    {
        throw new \Exception('Not implemented.');
    }

    public function patch()
    {
        throw new \Exception('Not implemented.');
    }

    private function request(string $method, string $path, array $options): Response
    {
        return $this->client->request($method, $path, $options);
    }

    private function handleResponse(Response $response): array
    {
        if ($response->getStatusCode() === SymfonyResponse::HTTP_UNAUTHORIZED) {
            throw new \Exception('Not authenticated. Run the "wethos:authenticate" command.');
        }

        $body = $response->getBody();

        if ($body instanceof Stream) {
            return $this->parseJson($response->getBody()->getContents());
        }

        return $this->parseJson($body);
    }

    private function parseJson(string $body): array
    {
        $data = json_decode($body, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Invalid JSON response.');
        }

        return $data['data'] ?? $data;
    }

    private function attemptRefresh(array $options): bool
    {
        $refreshToken = $this->tokenRepository->getRefreshToken();

        if (!$refreshToken) {
            return false;
        }

        $refreshTokenPayload = new RefreshTokenPayload($this->clientId, $this->clientSecret);
        $response            = $this->post('/oauth/token', $refreshTokenPayload->toArray(), $options);

        if ($response->getStatusCode() !== SymfonyResponse::HTTP_OK) {
            return false;
        }

        $data = json_decode($response, true);

        $this->tokenRepository->setAccessToken($data['access_token']);
        $this->tokenRepository->setRefreshToken($data['refresh_token']);

        return true;
    }
}
