<?php declare(strict_types=1);

namespace App\Command;

use App\Http\Transport;
use App\Repository\TokenRepository;
use App\Http\OAuth\AuthenticatePayload;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'wethos:authenticate',
    description: 'Authenticate with Wethos.co'
)]
class WethosAuthenticateCommand extends Command
{
    /** @var Transport */
    private $transport;

    /** @var TokenRepository */
    private $tokenRepository;

    /** @var int */
    private $clientId;

    /** @var string */
    private $clientSecret;

    public function __construct(Transport $transport, TokenRepository $tokenRepository, string $clientId, string $clientSecret, string $name = null)
    {
        parent::__construct($name);

        if (!$clientId || !$clientSecret) {
            throw new \Exception('Missing client id or client secret. Make sure these are configured correctly in your .env file.');
        }

        $this->transport       = $transport;
        $this->tokenRepository = $tokenRepository;
        $this->clientId        = $clientId;
        $this->clientSecret    = $clientSecret;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'Username')
            ->addArgument('password', InputArgument::REQUIRED, 'Password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Wethos Authenticate');

        $authPayload = new AuthenticatePayload($this->clientId, $this->clientSecret, $input->getArgument('username'), $input->getArgument('password'));

        $headers = [
            'Accept'       => Transport::HEADER_ACCEPT,
            'Content-Type' => Transport::HEADER_CONTENT_TYPE,
        ];

        $response = $this->transport->post('/oauth/token', $authPayload->toArray(), $headers);

        $this->tokenRepository->storeAccessToken($response['access_token']);
        $this->tokenRepository->storeRefreshToken($response['refresh_token']);

        $io->success('Authentication was successful. You may now retrieve your data using the "wethos" commands.');

        return Command::SUCCESS;
    }
}
