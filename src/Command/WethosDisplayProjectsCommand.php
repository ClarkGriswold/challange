<?php declare(strict_types=1);

namespace App\Command;

use App\Http\Transport;
use App\Repository\TokenRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'wethos:projects',
    description: 'Add a short description for your command',
)]
class WethosDisplayProjectsCommand extends Command
{
    /** @var Transport */
    private $transport;

    /** @var TokenRepository */
    private $tokenRepository;

    public function __construct(Transport $transport, TokenRepository $tokenRepository, string $name = null)
    {
        parent::__construct($name);

        $this->transport       = $transport;
        $this->tokenRepository = $tokenRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('project_id', InputArgument::OPTIONAL, 'Project ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $accessToken = $this->tokenRepository->getAccessToken();

        if (!$accessToken) {
            $io->error('Not authenticated. Run the "wethos:authenticate" command.');

            return Command::FAILURE;
        }

        $headers = [
            'Accept'           => Transport::HEADER_ACCEPT,
            'X-Requested-With' => Transport::HEADER_X_REQUESTED_WITH,
            'Authorization'    => sprintf('Bearer %s', $accessToken),
        ];

        try {
            $currentUser = $this->getCurrentUser($headers);
        } catch(\Exception $e) {
            return $this->handleError($io, $e);
        }

        // Display project path
        if ($projectId = (int) $input->getArgument('project_id')) {
            try {
                $project = $this->getProject($projectId, $headers);
            } catch (\Exception $e) {
                return $this->handleError($io, $e);
            }

            $io->title(sprintf('Wethos Display Project: %s', $project['id']));

            $this->displayProject($io, $project);
            $this->displayTeamLead($io, $project);
            $this->displaySummary($io, $project);

            $io->success('End of project. Thanks for viewing.');

            return Command::SUCCESS;
        }

        $io->title('Wethos Display Projects');

        // Display projects path
        try {
            $projects = $this->getProjects($currentUser, $headers);
        } catch(\Exception $e) {
            return $this->handleError($io, $e);
        }

        $this->displayProjects($io, $projects);

        $io->success('End of projects. Thanks for viewing.');

        return Command::SUCCESS;
    }

    private function handleError(SymfonyStyle$io, \Exception $e): int
    {
        $io->error($e->getMessage());

        return Command::FAILURE;
    }

    private function getCurrentUser(array $headers): array
    {
        return $this->transport->get('api/v2/currentuser', $headers);
    }

    private function getProjects(array $currentUser, $headers): array
    {
        $path   = 'api/v2/projects';
        $params = [
            'filter'   => null,
            'page'     => 1,
            'status'   => 'lead,kickoff,inprogress,completed,won,lost',
            'owner_id' => $currentUser['id'],
            'sort'     => 'name|asc'
        ];

        return $this->transport->get(sprintf('%s?%s', $path, http_build_query($params)), $headers);
    }

    private function getProject(int $proejctId, array $headers): array
    {
        return $this->transport->get(sprintf('api/v2/projects/%s', $proejctId), $headers);
    }

    private function displayProjects(SymfonyStyle $io, array $projects): void
    {
        $io->section('Projects');

        $rows = [];
        foreach ($projects as $project) {
            $rows[] = [
                $project['id'],
                $project['name'],
                $project['client_name'],
                $project['clientowner']['name'],
                $project['clientowner']['email'],
                sprintf('%s %s', $project['teamlead']['first_name'], $project['teamlead']['last_name']),
                $project['teamlead']['email'],
            ];
        }

        $io->table(
            ['Project ID', 'Project Name', 'Client Name', 'Client Owner Name', 'Client Owner Email', 'Team Lead Name', 'Team Lead Email'],
            $rows
        );
    }

    private function displayProject(SymfonyStyle $io, array $project): void
    {
        $io->section(sprintf('Project'));
        $io->table(
            ['Project Name', 'status', 'Client Name', 'Client Website URL'],
            [
                [$project['name'], $project['status'], $project['client']['name'], $project['client']['website_url']]
            ]
        );
    }

    private function displayTeamLead(SymfonyStyle $io, array $project): void
    {
        $io->section(sprintf('Team Lead'));
        $io->table(
            ['First Name', 'Last Name', 'Email'],
            [
                [$project['teamlead']['first_name'], $project['teamlead']['last_name'], $project['teamlead']['email']]
            ]
        );
    }

    private function displaySummary(SymfonyStyle $io, array $project): void
    {
        $io->section(sprintf('Summary'));
        $io->table(
            ['Total Billable', 'Income Remaining', 'Profit Projected', 'Price Total'],
            [
                [$project['summary']['total_billable'], $project['summary']['income_remaining'], $project['summary']['profit_projected'], $project['summary']['price_total']]
            ]
        );
    }
}
