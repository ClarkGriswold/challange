<?php declare(strict_types=1);

namespace App\Command;

use App\Http\Transport;
use App\Repository\TokenRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

#[AsCommand(
    name: 'wethos:profile',
    description: 'Displays infomation from your profile.',
)]
class WethosDisplayProfileCommand extends Command
{
    /** @var Transport */
    private $transport;

    /** @var TokenRepository */
    private $tokenRepository;

    public function __construct(Transport $transport, TokenRepository $tokenRepository, string $name = null)
    {
        parent::__construct($name);

        $this->transport       = $transport;
        $this->tokenRepository = $tokenRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Wethos Display Profile');

        $accessToken = $this->tokenRepository->getAccessToken();

        if (!$accessToken) {
            $io->error('Not authenticated. Run the "wethos:authenticate" command.');

            return Command::FAILURE;
        }

        $headers = [
            'Accept'           => Transport::HEADER_ACCEPT,
            'X-Requested-With' => Transport::HEADER_X_REQUESTED_WITH,
            'Authorization'    => sprintf('Bearer %s', $accessToken),
        ];

        try {
            $profile = $this->getProfile($headers);
        } catch(\Exception $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }

        $this->displayName($io, $profile);
        $this->displayShortBio($io, $profile);
        $this->displayAvailability($io, $profile);
        $this->displayLocation($io, $profile);
        $this->displayCauses($io, $profile);
        $this->displayIndustries($io, $profile);
        $this->displayTools($io, $profile);
        $this->displayRoles($io, $profile);
        $this->displayWork($io, $profile);
        $this->displaySocialLinks($io, $profile);

        $io->success('End of profile. Thanks for viewing.');

        return Command::SUCCESS;
    }

    private function getProfile(array $headers): array
    {
        return $this->transport->get('api/v2/currentspecialist', $headers);
    }

    private function displayName(SymfonyStyle $io, array $profile): void
    {
        $fullName = sprintf('%s %s', $profile['user']['first_name'], $profile['user']['last_name']);

        $io->section('Name');
        $io->table(
            ['First Name', 'Last Name', 'Title', 'Pronouns', 'Email'],
            [
                [$profile['user']['first_name'], $profile['user']['last_name'], $profile['title'], $profile['pronouns'], $profile['user']['email']],
            ]
        );
    }

    private function displayShortBio(SymfonyStyle $io, array $profile): void
    {
        $io->section('Short Bio');
        $io->text($profile['short_bio']);
    }

    private function displayAvailability(SymfonyStyle $io, array $profile): void
    {
        $io->section('Availability');
        $io->text($profile['availability']);
    }

    private function displayLocation(SymfonyStyle $io, array $profile): void
    {
        $fullName = sprintf('%s %s', $profile['user']['first_name'], $profile['user']['last_name']);

        $io->section('Location');
        $io->table(
            ['Name', 'Province', 'Country', 'Post Code', 'Latitude', 'Longitude'],
            [
                [$profile['location_name'], $profile['location_state'], $profile['location_country'], $profile['location_postcode'], $profile['location_lat'], $profile['location_lon']],
            ]
        );
    }

    private function displayCauses(SymfonyStyle $io, array $profile): void
    {
        $io->section('Causes');
        $io->listing(array_column($profile['causes'], 'name'));
    }

    private function displayIndustries(SymfonyStyle $io, array $profile): void
    {
        $io->section('Industries');

        $rows = [];
        foreach ($profile['industries'] as $industry) {
            $rows[] = [$industry['name'], $industry['experience']];
        }

        $io->table(
            ['Name', 'Experience'],
            $rows
        );
    }

    private function displayTools(SymfonyStyle $io, array $profile): void
    {
        $io->section('Tools');

        $rows = [];
        foreach ($profile['tools'] as $tool) {
            $rows[] = [$tool['name'], $tool['proficiency']];
        }

        $io->table(
            ['Name', 'Proficiency'],
            $rows
        );
    }

    private function displayRoles(SymfonyStyle $io, array $profile): void
    {
        $io->section('Roles');

        $rows = [];
        foreach ($profile['roles'] as $role) {
            $rows[] = [$role['name'], $role['description']];
        }

        $io->table(
            ['Name', 'Description'],
            $rows
        );
    }

    private function displayWork(SymfonyStyle $io, array $profile): void
    {
        $io->section('Work');

        $rows = [];
        foreach ($profile['work'] as $work) {
            $workIndustries = [];
            foreach ($work['industries'] as $workIndustry) {
                $workIndustries[] = $workIndustry['name'];
            }

            $tools = [];
            foreach ($work['tools'] as $tool) {
                $tools[] = $tool['name'];
            }

            $rows[] = [$work['name'], $work['client'], $work['role'], $work['linkurl'], implode(', ', $workIndustries), implode(', ', $tools)];
        }

        $io->table(
            ['Name', 'Client', 'Role', 'Link URL', 'Industries', 'Tools'],
            $rows
        );
    }

    private function displaySocialLinks(SymfonyStyle $io, array $profile): void
    {
        $io->section('Social Links');

        $rows = [];

        foreach ($profile['links'] as $link) {
            $rows[] = [$link['name'], $link['url']];
        }

        $io->table(
            ['Name', 'URL'],
            $rows
        );
    }
}
