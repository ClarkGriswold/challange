<?php declare(strict_types=1);

namespace App\Repository;

use Symfony\Component\Filesystem\Filesystem;

class TokenRepository
{
    private const STORAGE_PATH           = 'var/storage/tokens';
    private const ACCESS_TOKEN_FILENAME  = 'access-token.store';
    private const REFRESH_TOKEN_FILENAME = 'refresh-token.store';

    /** @var Filesystem */
    private $filesystem;

    public function __construct()
    {
        $this->filesystem = new Filesystem();

        if (!$this->filesystem->exists(self::STORAGE_PATH)) {
            $this->filesystem->mkdir(self::STORAGE_PATH);
        }
    }

    /**
     * @throws Symfony\Component\Filesystem\Exception\IOException
     */
    public function storeAccessToken(string $accessToken): void
    {
        $this->filesystem->dumpFile(sprintf('%s/%s', self::STORAGE_PATH, self::ACCESS_TOKEN_FILENAME), $accessToken);
    }

    public function getAccessToken(): string
    {
        return file_get_contents(sprintf('%s/%s', self::STORAGE_PATH, self::ACCESS_TOKEN_FILENAME));
    }

    /**
     * @throws Symfony\Component\Filesystem\Exception\IOException
     */
    public function storeRefreshToken(string $accessToken): void
    {
        $this->filesystem->dumpFile(sprintf('%s/%s', self::STORAGE_PATH, self::REFRESH_TOKEN_FILENAME), $accessToken);
    }

    public function getRefreshToken(): string
    {
        return file_get_contents(sprintf('%s/%s', self::STORAGE_PATH, self::REFRESH_TOKEN_FILENAME));
    }
}
